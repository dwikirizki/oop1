<?php
        require_once('animal.php');
        require_once('ape.php');
        require_once('frog.php');
        $animal = new Animal ("Shaun");
        echo "Name : ".$animal->name ."<br>";
        echo "Jumlah legs : " .$animal->legs ."<br>";
        echo "Cold Blooded : ".$animal -> cold_blooded ."<br><br>";

        $kodok = new frog("Buduk");
        echo "Name : ".$kodok->name ."<br>";
        echo "Jumlah legs : " .$kodok->legs ."<br>";
        echo "Cold Blooded : ".$kodok -> cold_blooded ."<br>";
        echo "Jump :" .$kodok -> jump ."<br><br>";

        $sungokong = new Ape("Kera Sakti");
        echo "Name : ".$sungokong->name ."<br>";
        echo "Jumlah legs : " .$sungokong->legs ."<br>";
        echo "Cold Blooded : ".$sungokong -> cold_blooded ."<br>";
        echo "Yell :" .$sungokong -> yell ."<br><br>";
        
        
?>